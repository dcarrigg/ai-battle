To add your AI into the battle:
1. Write your new AI script, using TestAI or RandomAI as examples
2. Create a scriptable object for your AI
3. Attach the scriptable object to the GameManager object in the list of combatants
4. Hit play

The game will start paused. To unpause the game, uncheck the Paused bool on the GameManager game object.

This is the exact project we will be using to battle our AI, but feel free to modify your own version for testing. For example, you can modify the code in the OnDrawGizmos function to draw any information your AI script might be currently storing.

Good luck!